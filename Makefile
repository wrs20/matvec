
CC:=icc
CFLAGS:=-shared -restrict -qopenmp -xHost -O3 -qopt-report=5 -fPIC
FLAGS:=

TARGETS:=matvec.so

test: ${TARGETS}
	python matvec.py

libs: ${TARGETS}

%.so: %.cpp
	${CC} $< -o $@ ${CFLAGS} ${LFLAGS}

.PHONY: clean
clean:
	rm ${TARGETS}
	



