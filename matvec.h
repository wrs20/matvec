
#define RESTRICT restrict
#define SIMD_LEN 8
#define NPICK 8


template<int NPRE>
static inline void template_matvec3(
    const int m,
    const int n,
    const double * RESTRICT A,
    const double * RESTRICT x0,
    const double * RESTRICT x1,
    double * RESTRICT b0,
    double * RESTRICT b1
){
    for(int vx=0 ; vx<m ; vx++){
        for(int cx=0 ; cx<NPRE ; cx++){
#pragma omp simd simdlen(SIMD_LEN)
            for(int rx=0 ; rx<NPRE ; rx++){
                b0[vx*NPRE+rx] += A[cx*NPRE + rx] * x0[vx*NPRE+cx];
                b1[vx*NPRE+rx] += A[cx*NPRE + rx] * x1[vx*NPRE+cx];
            }
        }
    }
};


template<int NPRE>
static inline void template_matvec0(
    const int m,
    const int n,
    const double * RESTRICT A,
    const double * RESTRICT x0,
    const double * RESTRICT x1,
    double * RESTRICT b0,
    double * RESTRICT b1
){
    for(int vx=0 ; vx<m ; vx++){
#pragma omp simd simdlen(SIMD_LEN)
        for(int rx=0 ; rx<NPRE ; rx++){
            double tmp0 = 0.0;
            double tmp1 = 0.0;
            for(int cx=0 ; cx<NPRE ; cx++){
                tmp0 += A[rx*NPRE + cx] * x0[vx*NPRE+cx];
                tmp1 += A[rx*NPRE + cx] * x1[vx*NPRE+cx];
            }
            b0[vx*NPRE+rx] = tmp0;
            b1[vx*NPRE+rx] = tmp1;
        }
    }
};
