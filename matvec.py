import ctypes
import time
import numpy as np
from math import *

INT = ctypes.c_int
REAL = ctypes.c_double

def PTR(arr):
    return arr.ctypes.get_as_parameter()


lib = ctypes.cdll.LoadLibrary('./matvec.so')
libd = {}
M = 5
for mx in range(M):
    libd[mx] = lib['matvec{}'.format(mx)]

def matvec(n=4, method=0):
    m = 10000


    A = np.array(np.random.uniform(size=(n,n)), dtype=REAL)
    A = np.tril(A) + np.tril(A, -1).T
    x0 = np.array(np.random.uniform(size=n*m), dtype=REAL)
    x1 = np.array(np.random.uniform(size=n*m), dtype=REAL)
    b0 = np.zeros_like(x0)
    b1 = np.zeros_like(x1)

    c0 = np.zeros_like(x0)
    c1 = np.zeros_like(x1)

    for mx in range(m):
        c0[mx*n:(mx+1)*n:] = np.matmul(A, x0[mx*n:(mx+1)*n:])
        c1[mx*n:(mx+1)*n:] = np.matmul(A, x1[mx*n:(mx+1)*n:])


    t0 = time.time()
    libd[method](INT(m), INT(n), PTR(A), PTR(x0), PTR(x1), PTR(b0), PTR(b1))
    t1 = time.time()

    err0 = np.linalg.norm(c0 - b0, np.inf)
    err1 = np.linalg.norm(c1 - b1, np.inf)
    if err0 > 10.**-12 or err1 > 10.**-12:
        print("errs:", err0, err1)


    tt = t1 - t0
    flop_count = 4
    flop_rate = flop_count * m * n * n / (tt*(10.**9))

    max_flop_rate = 2.6*32

    print("n: {:4d} |".format(n),
            "GFLOPs: {:6.2f}".format(flop_rate),
          "| {: 4.1f} %".format(100*flop_rate/max_flop_rate))
    return flop_rate
 
print(60*'-')
for mx in range(M):
    matvec(128, mx)

print(30*'-', 0, 30*'-')

prange = range(1, 30, 1)
quot = sum([px*px for px in prange])


for mx in range(M):
    av = 0.0
    for px in prange:
        av += matvec(px, mx)*(px*px)
    
    print(30*'-', mx+1, av/quot,30*'-')



