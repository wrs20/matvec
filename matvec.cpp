
#include "matvec.h"





extern "C" int matvec0(
    const int m,
    const int n,
    const double * RESTRICT A,
    const double * RESTRICT x0,
    const double * RESTRICT x1,
    double * RESTRICT b0,
    double * RESTRICT b1
){
    for(int vx=0 ; vx<m ; vx++){
#pragma omp simd simdlen(SIMD_LEN)
        for(int rx=0 ; rx<n ; rx++){
            double tmp0 = 0.0;
            double tmp1 = 0.0;
            for(int cx=0 ; cx<n ; cx++){
                tmp0 += A[rx*n + cx] * x0[vx*n+cx];
                tmp1 += A[rx*n + cx] * x1[vx*n+cx];
            }
            b0[vx*n+rx] = tmp0;
            b1[vx*n+rx] = tmp1;
        }
    }
    return 0;
}



extern "C" int matvec1(
    const int m,
    const int n,
    const double * RESTRICT A,
    const double * RESTRICT x0,
    const double * RESTRICT x1,
    double * RESTRICT b0,
    double * RESTRICT b1
){
    if (n == NPICK){
        for(int vx=0 ; vx<m ; vx++){
#pragma omp simd simdlen(SIMD_LEN)
            for(int rx=0 ; rx<NPICK ; rx++){
                double tmp0 = 0.0;
                double tmp1 = 0.0;
                for(int cx=0 ; cx<NPICK ; cx++){
                    tmp0 += A[rx*NPICK + cx] * x0[vx*NPICK+cx];
                    tmp1 += A[rx*NPICK + cx] * x1[vx*NPICK+cx];
                }
                b0[vx*NPICK+rx] = tmp0;
                b1[vx*NPICK+rx] = tmp1;
            }
        }
    } else {
        for(int vx=0 ; vx<m ; vx++){
#pragma omp simd simdlen(SIMD_LEN)
            for(int rx=0 ; rx<n ; rx++){
                double tmp0 = 0.0;
                double tmp1 = 0.0;
                for(int cx=0 ; cx<n ; cx++){
                    tmp0 += A[rx*n + cx] * x0[vx*n+cx];
                    tmp1 += A[rx*n + cx] * x1[vx*n+cx];
                }
                b0[vx*n+rx] = tmp0;
                b1[vx*n+rx] = tmp1;
            }
        }
    }
    return 0;
}


extern "C" int matvec2(
    const int m,
    const int n,
    const double * RESTRICT A,
    const double * RESTRICT x0,
    const double * RESTRICT x1,
    double * RESTRICT b0,
    double * RESTRICT b1
){
    for(int vx=0 ; vx<m ; vx++){

        for(int cx=0 ; cx<n ; cx++){
#pragma omp simd simdlen(SIMD_LEN)
            for(int rx=0 ; rx<n ; rx++){
                b0[vx*n+rx] += A[cx*n + rx] * x0[vx*n+cx];
                b1[vx*n+rx] += A[cx*n + rx] * x1[vx*n+cx];
            }
        }
    }
    return 0;
}


extern "C" int matvec3(
    const int m,
    const int n,
    const double * RESTRICT A,
    const double * RESTRICT x0,
    const double * RESTRICT x1,
    double * RESTRICT b0,
    double * RESTRICT b1
){
    if (n==8)       { template_matvec3<8>(m, n, A, x0, x1, b0, b1); }
    else if (n==9)  { template_matvec3<9>(m, n, A, x0, x1, b0, b1); }
    else if (n==10) { template_matvec3<10>(m, n, A, x0, x1, b0, b1); }
    else if (n==11) { template_matvec3<11>(m, n, A, x0, x1, b0, b1); }
    else if (n==12) { template_matvec3<12>(m, n, A, x0, x1, b0, b1); }
    else if (n==13) { template_matvec3<13>(m, n, A, x0, x1, b0, b1); }
    else if (n==14) { template_matvec3<14>(m, n, A, x0, x1, b0, b1); }
    else if (n==15) { template_matvec3<15>(m, n, A, x0, x1, b0, b1); }
    else if (n==16) { template_matvec3<16>(m, n, A, x0, x1, b0, b1); }
    else if (n==17) { template_matvec3<17>(m, n, A, x0, x1, b0, b1); }
    else if (n==18) { template_matvec3<18>(m, n, A, x0, x1, b0, b1); }
    else if (n==19) { template_matvec3<19>(m, n, A, x0, x1, b0, b1); }
    else if (n==20) { template_matvec3<20>(m, n, A, x0, x1, b0, b1); }
    else if (n==21) { template_matvec3<21>(m, n, A, x0, x1, b0, b1); }
    else if (n==22) { template_matvec3<22>(m, n, A, x0, x1, b0, b1); }
    else if (n==23) { template_matvec3<23>(m, n, A, x0, x1, b0, b1); }
    else if (n==24) { template_matvec3<24>(m, n, A, x0, x1, b0, b1); }
    else if (n==25) { template_matvec3<25>(m, n, A, x0, x1, b0, b1); }
    else if (n==26) { template_matvec3<26>(m, n, A, x0, x1, b0, b1); }
    else if (n==27) { template_matvec3<27>(m, n, A, x0, x1, b0, b1); }
    else if (n==28) { template_matvec3<28>(m, n, A, x0, x1, b0, b1); }
    else if (n==29) { template_matvec3<29>(m, n, A, x0, x1, b0, b1); }
    else {
        for(int vx=0 ; vx<m ; vx++){
            for(int cx=0 ; cx<n ; cx++){
#pragma omp simd simdlen(SIMD_LEN)
                for(int rx=0 ; rx<n ; rx++){
                    b0[vx*n+rx] += A[cx*n + rx] * x0[vx*n+cx];
                    b1[vx*n+rx] += A[cx*n + rx] * x1[vx*n+cx];
                }
            }
        }
    }

    return 0;
}


extern "C" int matvec4(
    const int m,
    const int n,
    const double * RESTRICT A,
    const double * RESTRICT x0,
    const double * RESTRICT x1,
    double * RESTRICT b0,
    double * RESTRICT b1
){
    if (n==8)       { template_matvec0<8>(m, n, A, x0, x1, b0, b1); }
    else if (n==9)  { template_matvec0<9>(m, n, A, x0, x1, b0, b1); }
    else if (n==10) { template_matvec0<10>(m, n, A, x0, x1, b0, b1); }
    else if (n==11) { template_matvec0<11>(m, n, A, x0, x1, b0, b1); }
    else if (n==12) { template_matvec0<12>(m, n, A, x0, x1, b0, b1); }
    else if (n==13) { template_matvec0<13>(m, n, A, x0, x1, b0, b1); }
    else if (n==14) { template_matvec0<14>(m, n, A, x0, x1, b0, b1); }
    else if (n==15) { template_matvec0<15>(m, n, A, x0, x1, b0, b1); }
    else if (n==16) { template_matvec0<16>(m, n, A, x0, x1, b0, b1); }
    else if (n==17) { template_matvec0<17>(m, n, A, x0, x1, b0, b1); }
    else if (n==18) { template_matvec0<18>(m, n, A, x0, x1, b0, b1); }
    else if (n==19) { template_matvec0<19>(m, n, A, x0, x1, b0, b1); }
    else if (n==20) { template_matvec0<20>(m, n, A, x0, x1, b0, b1); }
    else if (n==21) { template_matvec0<21>(m, n, A, x0, x1, b0, b1); }
    else if (n==22) { template_matvec0<22>(m, n, A, x0, x1, b0, b1); }
    else if (n==23) { template_matvec0<23>(m, n, A, x0, x1, b0, b1); }
    else if (n==24) { template_matvec0<24>(m, n, A, x0, x1, b0, b1); }
    else if (n==25) { template_matvec0<25>(m, n, A, x0, x1, b0, b1); }
    else if (n==26) { template_matvec0<26>(m, n, A, x0, x1, b0, b1); }
    else if (n==27) { template_matvec0<27>(m, n, A, x0, x1, b0, b1); }
    else if (n==28) { template_matvec0<28>(m, n, A, x0, x1, b0, b1); }
    else if (n==29) { template_matvec0<29>(m, n, A, x0, x1, b0, b1); }
    else {
        for(int vx=0 ; vx<m ; vx++){
            for(int cx=0 ; cx<n ; cx++){
#pragma omp simd simdlen(SIMD_LEN)
                for(int rx=0 ; rx<n ; rx++){
                    b0[vx*n+rx] += A[cx*n + rx] * x0[vx*n+cx];
                    b1[vx*n+rx] += A[cx*n + rx] * x1[vx*n+cx];
                }
            }
        }
    }

    return 0;
}





